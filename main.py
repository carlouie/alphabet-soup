"""
main.py Python 3.7.2
Carlouie Nievera
Description: 5 by 5 wordsearch puzzle input and location of word output
"""


all_content = []    # all content in input.txt


with open('input.txt', 'r') as puzzle: 
    for line in puzzle.read().split("\n"):
        all_contents = line.split(" ")
        all_content.append(line)

    word_s = all_content[-3:]
    letter_s = all_content[1:6]  

wordsearch = dict([((row,column),character) for row,line_s in enumerate(letter_s)
                                            for column,character in enumerate(line_s.split())])

path = []
def find_word(word, path):
    for letter in word:
        for row_col in wordsearch.keys():
            if row_col not in wordsearch.keys() or letter not in wordsearch[row_col]:
                pass
            elif row_col in wordsearch.keys(): 
                path.append(row_col)
                start_rc = str(path[0])
                end_rc = str(path[-1])


                start_rc_string = ''.join(start_rc.replace(",", ":"))
                end_rc_string = ''.join(end_rc.replace(",", ":"))

                last_start_rc = start_rc_string.strip(")")
                last_end_rc = end_rc_string.strip(")")

                clean_start = last_start_rc.replace("(", "")
                clean_end = last_end_rc.replace("(", "")

                actual_clean_start = clean_start.replace(" ", "")
                actual_clean_end = clean_end.replace(" ", "")

    print(word, actual_clean_start, actual_clean_end)


find_word(word_s[0], [])
find_word(word_s[1], [])
find_word(word_s[2], [])
